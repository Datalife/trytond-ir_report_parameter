# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest
import doctest
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.tests.test_tryton import suite as test_suite
from trytond.pool import Pool
from trytond.tests.test_tryton import doctest_checker
from trytond.tests.test_tryton import doctest_teardown


class IrReportParameterTestCase(ModuleTestCase):
    """Test Ir Report Parameter module"""
    module = 'ir_report_parameter'

    @with_transaction()
    def test_report_parameter_set(self):
        """Test report parameter set"""
        pool = Pool()
        ActionReport = pool.get('ir.action.report')
        Set = pool.get('ir.action.report.parameter.set')

        action, = ActionReport.search([
            ('report_name', '=', 'res.user.email_reset_password')])
        parameter_set, = Set.create([{
            'name': 'Set of parameter for report X',
            'parameters': [
                ('create', [
                    {
                        'name': 'param1',
                        'string': 'Param 1',
                        'type_': 'boolean'
                    }, {
                        'name': 'param2',
                        'string': 'Param 2',
                        'type_': 'numeric',
                        'digits': 2
                    }]
                )
            ]}
        ])
        ActionReport.write([action], {'parameter_set': parameter_set})


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            IrReportParameterTestCase))
    suite.addTests(doctest.DocFileSuite('scenario_report_parameter.rst',
            tearDown=doctest_teardown, encoding='utf-8',
            checker=doctest_checker,
            optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    return suite
