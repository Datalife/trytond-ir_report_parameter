# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import ir
from .ir import PrintReporWithParameterMixin, ModelParameterWithPartyMixin
from .party import ReportParameterContextMixin
from . import party


def register():
    Pool.register(
        ir.ActionReport,
        ir.ReportParameterSet,
        ir.ReportParameter,
        ir.ReportParameterParameterSet,
        ir.PrintReportWithParameter,
        party.ReportParameter,
        module='ir_report_parameter', type_='model')
