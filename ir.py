# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields, DictSchemaMixin
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.wizard import StateView, StateReport, Button, StateTransition
from trytond.i18n import gettext
from trytond.exceptions import UserError
from trytond.tools import cached_property


class ActionReport(metaclass=PoolMeta):
    __name__ = 'ir.action.report'

    parameter_set = fields.Many2One('ir.action.report.parameter.set',
        'Parameter Set')


class ReportParameterSet(ModelSQL, ModelView):
    """Action Report parameter Set"""
    __name__ = 'ir.action.report.parameter.set'

    name = fields.Char('Name', required=True, translate=True)
    parameters = fields.Many2Many('ir.action.report.parameter-parameter.set',
        'parameter_set', 'parameter', 'Parameters')


class ReportParameter(DictSchemaMixin, ModelSQL, ModelView):
    """Action Report parameter"""
    __name__ = 'ir.action.report.parameter'

    sets = fields.Many2Many('ir.action.report.parameter-parameter.set',
        'parameter', 'parameter_set', 'Sets')


class ReportParameterParameterSet(ModelSQL):
    """Action Report parameter - Set"""
    __name__ = 'ir.action.report.parameter-parameter.set'
    _table = 'ir_action_report_parameter_set_rel'

    parameter = fields.Many2One('ir.action.report.parameter', 'Parameter',
        ondelete='CASCADE', select=True, required=True)
    parameter_set = fields.Many2One('ir.action.report.parameter.set', 'Set',
        ondelete='CASCADE', select=True, required=True)


class PrintReportWithParameterMixin(object):

    parameter_set = fields.Many2One('ir.action.report.parameter.set',
        'Parameter Set')
    parameters = fields.Dict('ir.action.report.parameter', 'Parameters',
        domain=[
            ('sets', '=', Eval('parameter_set', -1)),
        ],
        depends=['parameter_set'],
        help="Add parameters to the report.")


class PrintReportWithParameter(PrintReportWithParameterMixin, ModelView):
    '''Print report with parameter'''
    __name__ = 'ir.action.report.print.parameter'


class PrintReporWithParameterMixin(object):

    start = StateTransition()
    parameters = StateView('ir.action.report.print.parameter',
        'ir_report_parameter.report_print_parameter', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True)]
    )
    print_ = StateReport('')

    def transition_start(self):
        action_report = self._get_action_report_id
        if not action_report.parameter_set:
            raise UserError(gettext(
                'ir_report_parameter.msg_missing_set',
                report=action_report.rec_name))
        if not action_report.parameter_set.parameters:
            return 'print_'
        return 'parameters'

    def default_parameters(self, fields):
        pool = Pool()
        Report = pool.get('ir.action.report')

        active_model = self.model
        report_name = self.__class__.print_.report_name
        if not active_model:
            report = Report.search([('report_name', '=', report_name)],
                limit=1)
            if report:
                active_model = pool.get(report[0].model)

        action_report = self._get_action_report_id
        res = {
            'parameter_set': action_report.parameter_set.id,
            'parameters': {p.name: None for p in
                action_report.parameter_set.parameters}
        }
        if active_model:
            if hasattr(active_model, '_party_report_parameter'):
                records = active_model.browse(
                    Transaction().context['active_ids'])
                res['parameters'].update(
                    active_model.get_party_report_parameters(
                        records, report_name))
        return res

    @cached_property
    def _get_action_report_id(self):
        ActionReport = Pool().get('ir.action.report')
        action_report = ActionReport.search([
            ('report_name', '=', self.__class__.print_.report_name)])
        if action_report:
            return action_report[0]

    def do_print_(self, action):
        Action = Pool().get('ir.action')

        action_report = self._get_action_report_id
        if action_report and action_report.action != action:
            action = action_report.action
            action = Action.get_action_values(action.type, [action.id])[0]
        data = getattr(self.parameters, 'parameters', {}).copy()
        # extra key to know in report get_context if parameters has been set
        data['parameter_set'] = True
        data.update({
            'id': Transaction().context['active_id'],
            'ids': Transaction().context['active_ids'],
        })
        return action, data


class ModelParameterWithPartyMixin(object):

    @classmethod
    def get_party_report_parameters(cls, records, report_name):
        Parameter = Pool().get('party.report_parameter')

        parties = [r._party_report_parameter for r in records]
        if len(set(parties)) <= 1:
            party_domain = [None]
            if parties:
                party_domain.append(parties[0])
            party_report = Parameter.search([
                    ('report.report_name', '=', report_name),
                    ('party', 'in', party_domain)
                ], order=[('party', 'ASC NULLS LAST')], limit=1)
            if party_report:
                party_report, = party_report
                return party_report.report_parameters
        return {}

    @property
    def _party_report_parameter(self):
        return self.party.id
