datalife_ir_report_parameter
============================

The ir_report_parameter module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-ir_report_parameter/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-ir_report_parameter)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
