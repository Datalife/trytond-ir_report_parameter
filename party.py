# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelSQL, fields, Exclude, ModelView
from trytond.pyson import Eval
from sql.conditionals import Coalesce
from sql.operators import Equal
from trytond.pool import Pool


class ReportParameter(ModelSQL, ModelView):
    """Party Report Parameters"""
    __name__ = 'party.report_parameter'

    party = fields.Many2One('party.party', 'Party')
    report = fields.Many2One('ir.action.report', 'Report',
        required=True, domain=[
            ('parameter_set', '!=', None),
        ])
    report_parameter_set = fields.Function(
        fields.Many2One('ir.action.report.parameter.set', 'Parameter Set'),
        'on_change_with_report_parameter_set')
    report_parameters = fields.Dict('ir.action.report.parameter',
        'Parameters', domain=[
            ('sets', '=', Eval('report_parameter_set', -1)),
        ], depends=['report_parameter_set'])

    @classmethod
    def __setup__(cls):
        super().__setup__()
        t = cls.__table__()

        cls._sql_constraints += [
            ('excl_party_report',
                Exclude(t, (Coalesce(t.party, 0), Equal), (t.report, Equal)),
                'ir_report_parameter.msg_party_report_unique'),
        ]

    @fields.depends('report', '_parent_report.parameter_set')
    def on_change_with_report_parameter_set(self, name=None):
        if self.report and self.report.parameter_set:
            return self.report.parameter_set.id


class ReportParameterContextMixin(object):

    @classmethod
    def get_context(cls, records, header, data):
        pool = Pool()

        report_context = super().get_context(records, header, data)

        if not data.get('parameter_set', False) and records:
            model_name = records[0].__name__
            Model = pool.get(model_name)
            data.update(
                Model.get_party_report_parameters(records, cls.__name__))
        return report_context
